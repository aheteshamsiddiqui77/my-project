FROM centos:7
RUN yum install httpd -y
#COPY index.html /var/www/html/index.html
ADD https://s3-us-west-2.amazonaws.com/studentapi-cit/index.html /var/www/html/index.html
RUN chmod 755 /var/www/html/index.html
#CMD httpd -DFOREGROUND
RUN echo "demo" > /file1
ENTRYPOINT ["httpd", "-D", "FOREGROUND"]